#!/usr/bin/env python3

# MIT License
#
# Copyright (c) 2020 Adam S
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import argparse
from pathlib import Path
import re
import sys
from progress.bar import IncrementalBar
import concurrent.futures
import subprocess
import shutil


def exit_with_code(code):
    print('Exiting.')
    sys.exit(code)


def run_ffmpeg(cmd, args, bar):
    if args.verbose:
        subprocess.run(cmd, check=True)
    else:
        subprocess.run(cmd, check=True,
                       stdout=subprocess.DEVNULL,
                       stderr=subprocess.DEVNULL,
                       )
    bar.next()


parser = argparse.ArgumentParser(
    description='Convert loseless audio to opus loseless.')
parser.add_argument('path')
parser.add_argument("-v", "--verbose", help="increase output verbosity",
                    action="store_true")

args = parser.parse_args()

path = Path(args.path).resolve()
print(path)

if not path.is_dir():
    print('ERROR: path is not a directory')
    exit_with_code(1)

converted_dir = path / 'converted'
converted_dir.mkdir(exist_ok=True)

file_list = [f for f in path.glob('**/*.flac')
             if f.is_file()]

dir_set = {d.parent for d in file_list}

cover_set = set()
for _dir in dir_set:
    for cover in _dir.glob('*.png'):
        cover_set.add(cover)
    for cover in _dir.glob('*.jpg'):
        cover_set.add(cover)

bar = IncrementalBar('Progress', max=len(file_list))

with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:

    for idx, _file in enumerate(file_list, 1):
        #print(f"Processing {idx}/{len(file_list)}")

        rel_path = str(_file).replace(str(path), '')
        rel_path = 'converted/' + rel_path[1:]
        path_obj = path / rel_path
        path_dir = path_obj.parent

        path_dir.mkdir(parents=True, exist_ok=True)

        reg = re.compile(r'\.flac$')
        new_filename = reg.sub('.ogg', rel_path)
        new_filename = str(path) + '/' + new_filename

        cmd = ['ffmpeg', '-i', str(_file), '-q',
               '5.5', '-out_sample_rate', '48k', '-c:a',
               'libvorbis', '-map', '0:a', new_filename]

        executor.submit(run_ffmpeg, cmd, args, bar)

        if args.verbose:
            print(cmd)
            print('\n')

    bar.finish()

for cover in cover_set:
    rel_cover_path = 'converted/' + str(cover).replace(str(path), '')[1:]
    cover_path_obj = path / rel_cover_path
    cover_path_dir = cover_path_obj.parent
    
    shutil.copy(cover, cover_path_obj)
    
    if args.verbose:
        print('Copied', str(cover), 'to', str(cover_path_obj))